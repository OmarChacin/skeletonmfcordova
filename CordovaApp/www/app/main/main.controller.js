angular
  .module('app.main')
  .controller('MainCtrl', ['$cordovaBarcodeScanner', '$log', MainCtrl]);


/* @ngInject */
function MainCtrl($cordovaBarcodeScanner, $log) {
  /* jshint validthis: true */
  var vm = this;

  vm.activate = activate;
  vm.title = 'MainCtrl';

  activate();

  ////////////////

  function activate() {
    $log.info('Main Controller has started');
  }

  vm.vibrate = vibrate;
  vm.getPicture = getPicture;
  vm.getRSSFeed = getRSSFeed;
  vm.barcode = barcode;
  vm.clickMe = clickMe;

  // Trigger the vibration
  function vibrate(){
    navigator.vibrate(3000);
  }
  // Trigger the camera
  function getPicture(){
    navigator.camera.getPicture(getPictureSuccess, getPictureFail, { quality: 50,
      destinationType: Camera.DestinationType.FILE_URI });
  }
  // Receive the result from the camera
  function getPictureSuccess(imageURI){
    $log.info("getPicture success "+imageURI);
    document.getElementById("image").src=imageURI;
  }
  // Called when some error occur with the camera
  function getPictureFail(){
    WL.Logger.error("getPicture failed");
  }
  // Execute a request to RSSAdapter/getStories
  function getRSSFeed(){
    var resourceRequest = new WLResourceRequest(
      "/adapters/RSSAdapter/getStories",
      WLResourceRequest.GET);
    resourceRequest.send().then(getRSSFeedSuccess,getRSSFeedError);
  }

  // Trigger camera to scan
  function barcode() {
    document.addEventListener("deviceready", function () {

      $cordovaBarcodeScanner
        .scan()
        .then(function(barcodeData) {
          // Success! Barcode data is here
          $log.log( 'Data ', barcodeData);
          vm.text = barcodeData.text;
        }, function(error) {
          // An error occurred
          $log.error('Error ', error);
        });
    })
  }

  // Receive the response from RSSAdapter
  function getRSSFeedSuccess(response){
    WL.Logger.info("getRSSFeedsSuccess");
    //The response.responseJSON contains the data received from the backend
    alert("Total RSS Feed items received:"+response.responseJSON.rss.channel.item.length);
  }

  // Called when some error occur during the request to RSSAdapter
  function getRSSFeedError(response){
    WL.Logger.error("Response ERROR:"+JSON.stringify(response));
    alert("Response ERROR:"+JSON.stringify(response));
  }

  function clickMe() {
    console.log('You click me!');
  }


}



